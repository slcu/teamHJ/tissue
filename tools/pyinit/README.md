A mesh (any format -- ply ory anything that can be read with Pyvista) to tissue init format converter in Python.
It can be used as a library (init.py) or through the command line (converter.py)