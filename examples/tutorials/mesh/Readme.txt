This directory holds some files with examples of how tissue describes geometries via meshes.
More information is found on the tissue wiki tutorial page

https://gitlab.com/slcu/teamHJ/tissue/wikis/Tutorials

Creator/author/contact: henrik.jonsson@slcu.cam.ac.uk
