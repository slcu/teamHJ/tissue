This directory has some files with examples of how growth and division
is implemented in tissue models.

More information is found on the tissue wiki tutorial page

https://gitlab.com/slcu/teamHJ/tissue/wikis/Tutorials

Creator/author/contact: henrik.jonsson@slcu.cam.ac.uk
